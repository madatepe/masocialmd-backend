const sql = require("./db.js");

// constructor
const Post = function(post) {
  this.is_delete = post.is_delete;
  this.is_fixed = post.is_fixed;
  this.post_type = post.post_type;
  this.user_id = post.user_id;
  this.file_id = post.file_id;
  this.description = post.description;
};

Post.create = (newPost, result) => {
  sql.query("INSERT INTO posts SET ?", newPost, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created post: ", { id: res.id, ...newPost });
    result(null, { id: res.id, ...newPost });
  });
};

Post.findById = (postId, result) => {
  sql.query(`SELECT * FROM posts WHERE id = ${postId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found post: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Post with the id
    result({ kind: "not_found" }, null);
  });
};

Post.getAll = result => {
  sql.query("SELECT * FROM posts", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("posts: ", res);
    result(null, res);
  });
};


Post.updateById = (id, post, result) => {
  sql.query(
    "UPDATE posts SET is_delete = ?, is_fixed = ?, post_type = ?, user_id = ?, file_id = ?, description = ? WHERE id = ?", [
      post.is_delete,
      post.is_fixed,
      post.post_type,
      post.user_id,
      post.file_id,
      post.description,
      id,
    ],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Post with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated post: ", { id: id, ...post });
      result(null, { id: id, ...post });
    }
  );
};

Post.remove = (id, result) => {
  sql.query("DELETE FROM posts WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Post with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted post with id: ", id);
    result(null, res);
  });
};

module.exports = Post;